﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;
using TMPro;

public class PlayerController : MonoBehaviour
{
    public float speed = 0;
    public TextMeshProUGUI countText;
    public TextMeshProUGUI livesText;
    public GameObject winTextObject;
    public GameObject loseTextObject;


    public GameObject player;
    public GameObject spawnablePrefab;
    public GameObject enemy;

    private Rigidbody rb;
    private int count;
    private int life;

    private float movementX;
    private float movementY;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();

        count = 0;
        life = 5;

        SetCountText();
        SetLifeText();

        winTextObject.SetActive(false);
        loseTextObject.SetActive(false);
    }

    private void OnMove(InputValue movementValue)
    {
        Vector2 movementVector = movementValue.Get<Vector2>();

        movementX = movementVector.x;
        movementY = movementVector.y;
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();

        if (count >= 50)
        {
            // Set the text value of your 'winText'
            winTextObject.SetActive(true);
        }
    }

    void SetLifeText()
    {
        livesText.text = "Lives: " + life.ToString();

        if (life <= 0)
        {
            //Debug.Log("you lose bitch");
            //loseTextObject.SetActive(true);
            SceneManager.LoadScene(2);
            //Destroy(player);
            // Set the text value of your 'winText'
            //winTextObject.SetActive(true);
        }
    }

    private void FixedUpdate()
    {
        Vector3 movement = new Vector3(movementX, 0.0f, movementY);

        rb.AddForce(movement * speed);

        //Debug.Log("player position: " + transform.position);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pickup"))
        {
            other.gameObject.SetActive(false);

            //Debug.Log("yo!");

            count = count + 1;
            SetCountText();
            speed++;


            Vector3 randomPosition = new Vector3(Random.Range(-5f, 5f), spawnablePrefab.transform.position.y, Random.Range(-5f, 5f));
            Instantiate(spawnablePrefab, randomPosition, spawnablePrefab.transform.rotation);

            if (count % 3 == 0)
            {
                randomPosition = new Vector3(Random.Range(-5f, 5f), enemy.transform.position.y, Random.Range(-5f, 5f));
                Instantiate(enemy, randomPosition, enemy.transform.rotation);
            }
        }

        if (other.gameObject.CompareTag("Enemy"))
        {
            other.gameObject.SetActive(false);
            //count = count - 1;
            //SetCountText();
            //Debug.Log("hey!");

            life--;
            SetLifeText();


        }
    }

}